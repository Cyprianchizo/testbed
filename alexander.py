"""Implements greetings from Alexander"""

from greetings import make_greeting


def greetings_from_alexander():
    """Prints greetings from Alexander.

    There isn't really anything else to say. Note that this will print text to
    the terminal as a side-effect.

    See Also
    --------
    greetings.make_greeting : This just just a thin wrapper around that function.
    """
    print(make_greeting("Alexander", "PG MABA"))


if __name__ == "__main__":
    greetings_from_alexander()
